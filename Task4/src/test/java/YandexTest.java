import patterns.singleton.WebDriverSingleton;
import org.openqa.selenium.WebDriver;
import org.testng.Assert;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import pages.YandexDraftEmailPage;
import pages.YandexEmailListPage;
import pages.YandexInboxPage;
import pages.YandexSignInPage;
import model.Email;
import model.*;

public class YandexTest {
    private static final String START_URL = "https://mail.yandex.by";
    private WebDriver driver= WebDriverSingleton.getWebDriverInstance();
    private User user = new User();
    private Email email = new Email();

    @BeforeClass(description = "Open YANDEX home page")
    public void openYandex() throws Exception {
        driver.get(START_URL);
    }

    @Test(description = "Login to YANDEX")
    public void loginToYandex() {
        boolean loginIsComplete = new YandexSignInPage(driver)
                .loginToYandex(user.getUserName(), user.getPassword())
                .isDisplayed(user.getFullUserName());
        Assert.assertTrue(loginIsComplete, "Looks you are NOT logged in correctly");
    }

    @Test(dependsOnMethods = "loginToYandex", description = "Create draft email")
    public void createDraftMail() {
        boolean draftIsCreated = new YandexInboxPage(driver).navigateToDraftEmailPage().
                createNewMail(email.getRecipient(), email.getSubject(), email.getBody()).isEmailDisplayed(email.getSubject());
        Assert.assertTrue(draftIsCreated, "Email is NOT created");
    }

    @Test(dependsOnMethods = "createDraftMail", description = "Verify data in draft email")
    public void verifyDraftEmailData() {
        boolean recipientIsCorrect = new YandexEmailListPage(driver).navigateToDraftEmail(email.getSubjectLocator()).
                draftMailRecipientEquals(user.getRecipientDraftMail());
        Assert.assertTrue(recipientIsCorrect, "Recipient is NOT valid");

        boolean subjectIsCorrect = new YandexDraftEmailPage(driver).draftMailSubjectEquals(email.getSubject());
        Assert.assertTrue(subjectIsCorrect, "Subject is NOT valid");

        boolean bodyIsCorrect = new YandexDraftEmailPage(driver).draftMailBodyEquals(email.getBody());
        Assert.assertTrue(bodyIsCorrect, "Body is NOT valid");
    }

    @Test(dependsOnMethods = "verifyDraftEmailData", description = "Sent email")
    public void sentEmail() {
        boolean isEmailSent = new YandexDraftEmailPage(driver).sendEmail().isEmailSent();
        Assert.assertTrue(isEmailSent, "Email is NOT sent");
    }

    @Test(dependsOnMethods = "sentEmail", description = "Verify email is displayed in Sent email folder")
    public void verifySentFolder() {
        Assert.assertTrue(isEmailExists(), "Email with the subject doesn't exist in the folder");
    }

    @Test(dependsOnMethods = "verifySentFolder", description = "Verify that email is deleted from sent folder")
    public void deleteSentEmailViaContextMenu() {
        if (driver.getClass().toString().contains("Chrome")) {
            new YandexEmailListPage(driver).deleteEmailViaContextMenu(email.getSubjectLocator());
            Assert.assertTrue(!isEmailExists(), "Email with the subject exists in the folder");
        } else if (driver.getClass().getName().contains("firefox")) {
            System.out.println("deleteSentEmailViaContextMenu test is skipped for FireFox");
        }
    }

    @Test(dependsOnMethods = "deleteSentEmailViaContextMenu", description = "Verify email is NOT displayed in DRAFT folder")
    public void verifyDraftFolder() {
        boolean draftEmailExist = new YandexEmailListPage(driver)
                .navigateToDraftFolder().isEmailDisplayed(email.getSubject());
        Assert.assertTrue(!draftEmailExist, "Email is displayed");
    }

    @Test(dependsOnMethods = "verifyDraftFolder", description = "Logout from YANDEX")
    public void logOff() {
        boolean isLogOff = new YandexEmailListPage(driver).clickOnLogOff().isLogOff();
        Assert.assertTrue(isLogOff, "Looks you are NOT logged off correctly");
    }

    @AfterClass(description = "Stop Browser")
    public void stopBrowser() {
        driver.close();
    }

    private boolean isEmailExists() {
        return new YandexInboxPage(driver).navigateToSentFolder().isEmailDisplayed(email.getSubject());
    }
}