package model;

public class User {
    private String userName = "testCDPHanna";
    private String password = "test123";
    private String fullUserName = "testCDPHanna@yandex.ru";
    private String recipientDraftMail = "hanna_kurak";


    public String getUserName() {
        return userName;
    }

    public String getFullUserName() {
        return fullUserName;
    }

    public String getPassword() {
        return password;
    }

    public String getRecipientDraftMail() {
        return recipientDraftMail;
    }
}
