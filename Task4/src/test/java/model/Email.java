package model;

import org.openqa.selenium.By;
import util.StringGeneratorUtils;

public class Email {

    private String recipient = "hanna_kurak@epam.com";
    private String body = "simple test";

    private String subject;
    private By subjectLocator;

    public Email() {
        subjectInit();
    }

    public String getRecipient() {
        return recipient;
    }

    public String getBody() {
        return body;
    }

    public String getSubject() {
        return subject;
    }

    public By getSubjectLocator() {
        return subjectLocator;
    }

    public void subjectInit() {
        subject = createSubject();
        System.out.println("Subject:   " + subject);
        subjectLocator = By.xpath(createSubjectLocator());
    }

    private String createSubject() {
        return StringGeneratorUtils.getRandomString(20);
    }

    private String createSubjectLocator() {
        String subjectLocator = "(//span[@class='mail-MessageSnippet-Item mail-MessageSnippet-Item_subject']/span[@title='";
        subjectLocator = subjectLocator + subject + "'])[1]";
        return subjectLocator;
    }


}
