package pages;


import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;


public class YandexInboxPage extends YandexPage {
    @FindBy(xpath = "//div[@class='mail-User-Name']")
    private WebElement headerUserName;

    @FindBy(xpath = "//a[@data-key='view=toolbar-button-compose-go&id=compose-go']")
    private WebElement writeIcon;

    @FindBy(xpath = "//div[@data-key='box=left-box']//a[@href='#sent']")
    private WebElement sentFolderLink;

    @FindBy(xpath = "//div[@class='mail-Done js-done']/div[@class='mail-Done-Title js-title-info']")
    private WebElement mailDoneLabel;

    public YandexInboxPage(WebDriver driver) {
        super(driver);
    }

    public YandexDraftEmailPage navigateToDraftEmailPage() {
        writeIcon.click();
        System.out.println("Draft folder is opened");
        return new YandexDraftEmailPage(driver);
    }

    public boolean isDisplayed(String userName) {
        return headerUserName.getText().equals(userName);
    }

    public YandexEmailListPage navigateToSentFolder() {
        sentFolderLink.click();
        try {
            Thread.sleep(5000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        System.out.println("Sent folder is opened");
        return new YandexEmailListPage(driver);
    }

    public boolean isEmailSent() {
        return mailDoneLabel != null;
    }
}
