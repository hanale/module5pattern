package pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;

public class YandexDraftEmailPage extends YandexPage {

    @FindBy(name = "to")
    private WebElement recipientField;

    @FindBy(name = "subj")
    private WebElement subjectField;

    @FindBy(xpath = "//div[contains(@class,'cke_contents cke_reset')]/div[@role='textbox']")
    private WebElement bodyField;

    @FindBy(xpath = "//div[@data-key='box=left-box']//a[@href='#draft']")
    private WebElement draftLink;

    @FindBy(css = "button.nb-button._nb-small-action-button._init.nb-with-s-right-gap.js-resolve")
    private WebElement saveButton;

    @FindBy(xpath = "//a[@data-key='view=toolbar-button-compose-go&id=compose-go']")
    private WebElement writeIcon;

    @FindBy(xpath = "//span[@class='mail-Bubble-Block']/span[@class='mail-Bubble-Block_text']")
    private WebElement recipientFieldDraftEmail;

    @FindBy(xpath = "//div[contains(@class,'cke_contents cke_reset')]/div[@role='textbox']/div")
    private WebElement bodyFieldDraftEmail;

    @FindBy(xpath = "//input[@name='subj']")
    private WebElement subjectFieldDraftEmail;

    @FindBy(xpath = "//button[@type='submit']")
    private WebElement sendButton;

    @FindBy(xpath = "//span[@title='hanna_kurak']")
    private WebElement recipientFromList;


    public YandexDraftEmailPage(WebDriver driver) {
        super(driver);
    }

    public YandexEmailListPage createNewMail(String recepient, String subject, String body) {
        String driverClassName = driver.getClass().toString().toLowerCase();
        if (driverClassName.contains("chrome")) {
            new Actions(driver).doubleClick(recipientField).click(recipientFromList).build().perform();
        } else if (driverClassName.contains("firefox")) {
            recipientField.click();
            recipientField.sendKeys(recepient);
        } else {
            try {
                throw new Exception("Unsupported driver.");
            } catch (Exception e) {
                e.printStackTrace();
            }
            //   System.out.println("Unsupported driver.");
            //   throw  new IllegalArgumentException("Unsupported driver.");
        }

        subjectField.click();
        subjectField.sendKeys(subject);
        bodyField.click();
        bodyField.sendKeys(body);
        draftLink.click();
        saveButton.click();
        try {
            Thread.sleep(6000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        System.out.println("New draft email is created");
        return new YandexEmailListPage(driver);
    }

    public YandexInboxPage sendEmail() {
        sendButton.click();
        System.out.println("Email is sent");
        return new YandexInboxPage(driver);
    }

    public boolean draftMailRecipientEquals(String recipient) {
        if (recipientFieldDraftEmail.getText().equals(recipient)) {
            System.out.println("Recipient is correct: " + recipient);
            return true;
        } else return false;
    }

    public boolean draftMailSubjectEquals(String subject) {
        if (subjectFieldDraftEmail.getAttribute("value").equals(subject)) {
            System.out.println("Subject is correct");
            return true;
        } else return false;
    }

    public boolean draftMailBodyEquals(String body) {
        if (bodyFieldDraftEmail.getText().equals(body)) {
            System.out.println("Body is correct");
            return true;
        } else return false;
    }

}
