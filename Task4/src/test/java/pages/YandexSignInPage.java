package pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class YandexSignInPage extends YandexPage {

    @FindBy(xpath = "//input[@name='login']")
    private WebElement loginInput;

    @FindBy(xpath = "//input[@name='passwd']")
    private WebElement passwordInput;

    @FindBy(xpath = "//span/button[@type='submit']")
    private WebElement submitButton;

    @FindBy(xpath = "//div[@class='domik2__submit']")
    private WebElement loginButton;

    public YandexSignInPage(WebDriver driver) {
        super(driver);
    }

    public YandexInboxPage loginToYandex(String user, String password) {
        loginInput.sendKeys(user);
        passwordInput.sendKeys(password);
        submitButton.click();
        System.out.println("You are logged in to Yandex");
        return new YandexInboxPage(driver);
    }

    public boolean isLogOff() {
        if (loginButton != null) {
            System.out.println("Email is sent");
            return true;
        } else return false;
    }
}
