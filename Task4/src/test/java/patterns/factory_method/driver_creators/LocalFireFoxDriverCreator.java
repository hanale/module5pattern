package patterns.factory_method.driver_creators;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.firefox.FirefoxProfile;


public class LocalFireFoxDriverCreator implements DriverCreator {

    private final String filePath;

    public LocalFireFoxDriverCreator(String path) {
        filePath = path;
    }


    @Override
    public WebDriver createDriver() {
        System.out.println("Firefox driver");
        System.setProperty("webdriver.gecko.driver", filePath);
        FirefoxProfile profile = new FirefoxProfile();
        profile.setEnableNativeEvents(true);
        return new FirefoxDriver(profile);
    }
}
