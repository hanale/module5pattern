package patterns.factory_method.driver_creators;


import org.openqa.selenium.Platform;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.remote.RemoteWebDriver;

import java.net.MalformedURLException;
import java.net.URL;

public class RemoteChromeDriverCreator implements DriverCreator {
    private final String urlPath;

    public RemoteChromeDriverCreator(String url) {
        urlPath = url;
    }

    @Override
    public WebDriver createDriver() throws Exception {
        System.out.println("Remote: chrome ");
        DesiredCapabilities capabilities;
        capabilities = DesiredCapabilities.chrome();
        capabilities.setPlatform(Platform.WINDOWS);
        try {
            return new RemoteWebDriver(new URL(urlPath), capabilities);
        } catch (MalformedURLException e) {
            throw new Exception("Create remote chrome driver failed", e);
        }
    }
}
