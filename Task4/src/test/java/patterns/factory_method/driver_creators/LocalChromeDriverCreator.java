package patterns.factory_method.driver_creators;

import org.openqa.selenium.WebDriver;
import patterns.decorator.ChromeDecorator;

public class LocalChromeDriverCreator implements DriverCreator {
    private final String filePath;

    public LocalChromeDriverCreator(String path) {
        filePath = path;
    }

    @Override
    public WebDriver createDriver() {
        System.out.println("Chrome driver");
        System.setProperty("webdriver.chrome.driver", filePath);
        return new ChromeDecorator();
    }
}
