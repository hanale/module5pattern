package patterns.factory_method.driver_creators;

import org.openqa.selenium.Platform;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.firefox.FirefoxProfile;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.remote.RemoteWebDriver;

import java.net.MalformedURLException;
import java.net.URL;

public class RemoteFireFoxDriverCreator implements DriverCreator {
    private final String urlPath;

    public RemoteFireFoxDriverCreator(String url) {
        urlPath = url;
    }

    @Override
    public WebDriver createDriver() {
        System.out.println("Remote: firefox ");
        DesiredCapabilities capabilities;
        FirefoxProfile profile = new FirefoxProfile();
        profile.setEnableNativeEvents(true);
        capabilities = DesiredCapabilities.firefox();
        capabilities.setPlatform(Platform.WINDOWS);
        capabilities.setCapability(FirefoxDriver.PROFILE, profile);
        try {
            return new RemoteWebDriver(new URL(urlPath), capabilities);
        } catch (MalformedURLException e) {
            e.printStackTrace();
        }
        return null;
    }
}
