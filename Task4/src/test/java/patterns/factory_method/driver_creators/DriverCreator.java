package patterns.factory_method.driver_creators;

import org.openqa.selenium.WebDriver;

public interface DriverCreator {
    WebDriver createDriver() throws Exception;
}
