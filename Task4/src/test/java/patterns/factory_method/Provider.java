package patterns.factory_method;

import org.openqa.selenium.WebDriver;
import patterns.factory_method.driver_creators.*;

import java.util.concurrent.TimeUnit;

public class Provider {
    private static final String LOCAL_HOST = "http://localhost:4444/wd/hub";
    private static final String CHROME_DRIVER_PATH = "./my_web_driver/chromedriver.exe";
    private static final String GECKO_DRIVER_PATH = "./my_web_driver/geckodriver.exe";

    public static WebDriver createDriverInstance(WebDriverType type) {
        WebDriver driver = null;
        DriverCreator driverCreator = null;
        switch (type) {
            case LocalChromeDriver:
                driverCreator = new LocalChromeDriverCreator(CHROME_DRIVER_PATH);
                break;
            case RemoteChromeDriver:
                driverCreator = new RemoteChromeDriverCreator(LOCAL_HOST);
                break;
            case RemoteFirefoxDriver:
                driverCreator = new RemoteFireFoxDriverCreator(LOCAL_HOST);
                break;
            case LocalFirefoxDriver:
                driverCreator = new LocalFireFoxDriverCreator(GECKO_DRIVER_PATH);
                break;
            default:
                throw new IllegalArgumentException("Choose valid driver");
        }

        try {
            driver = driverCreator.createDriver();
        } catch (Exception e) {
            e.printStackTrace();
        }
        addImplicitly(driver);
        return driver;
    }

    public static WebDriver createDriverInstance() {
        return createDriverInstance(WebDriverType.LocalChromeDriver);
    }

    private static void addImplicitly(WebDriver driver) {
        driver.manage().timeouts().implicitlyWait(100, TimeUnit.SECONDS);
        // Maximize browser window
        driver.manage().window().maximize();
    }
}
