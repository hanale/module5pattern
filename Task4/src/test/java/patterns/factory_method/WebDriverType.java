package patterns.factory_method;

public enum WebDriverType {
    LocalChromeDriver,
    LocalFirefoxDriver,
    RemoteChromeDriver,
    RemoteFirefoxDriver
}
