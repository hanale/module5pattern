package patterns.decorator;

import org.openqa.selenium.chrome.ChromeDriver;

public class ChromeDecorator extends ChromeDriver {

    @Override
    public void close() {
        System.out.println("Close Chrome browser");
        super.close();
    }

    @Override
    public void quit() {
        System.out.println("Quit Chrome browser");
        super.quit();
    }
}
