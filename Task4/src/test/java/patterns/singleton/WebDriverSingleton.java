package patterns.singleton;

import patterns.factory_method.Provider;
import org.openqa.selenium.WebDriver;

public class WebDriverSingleton {

    private static volatile WebDriver driver;
    private static final Object syncObject = new Object();

    private WebDriverSingleton() {
    }

    public static WebDriver getWebDriverInstance() {
        if (driver == null) {
            synchronized (syncObject) {
                if (driver == null) {
                    driver = Provider.createDriverInstance();
                }
            }
        }
        return driver;
    }

}
